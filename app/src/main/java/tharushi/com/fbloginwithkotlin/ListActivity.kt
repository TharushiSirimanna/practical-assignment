package tharushi.com.fbloginwithkotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.activity_list.*
import retrofit2.Call
import retrofit2.Callback
import tharushi.com.fbloginwithkotlin.SharedPreference.Companion.KEY_EMAIL
import tharushi.com.fbloginwithkotlin.SharedPreference.Companion.KEY_NAME
import tharushi.com.fbloginwithkotlin.model.Item
import tharushi.com.fbloginwithkotlin.model.Response

const val EXTRA_ITEM = "extra_item"

class ListActivity : AppCompatActivity(), Callback<Response>, Adapter.AdapterCallback {

    private lateinit var adapter: Adapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        var sharedPreference = SharedPreference(this)

        var name = sharedPreference.getValueString(SharedPreference.KEY_NAME)
        var email =  sharedPreference.getValueString(SharedPreference.KEY_EMAIL)

        tv_username.text = name
        tv_email.text = email


        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_list.layoutManager = linearLayoutManager // adapter set to the recyclerview

        adapter = Adapter(mutableListOf())
        rv_list.adapter = adapter
        adapter.callback =this




        btn_logout.setOnClickListener {

            LoginManager.getInstance().logOut()
            val intent = Intent(this@ListActivity, MainActivity::class.java)
            startActivity(intent)
            this@ListActivity.finish()

        }

        val newApiService = ApiService.Creator.newApiService()
        newApiService.userLogIn().enqueue(this)

    }


    override fun onFailure(call: Call<Response>, t: Throwable) {

    }

    override fun onClickViewCallback(item: Item) {
        val intent = Intent(this@ListActivity, DetailsActivity::class.java)
        intent.putExtra(EXTRA_ITEM, item)
        startActivity(intent)

    }

    override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
        if (response.isSuccessful) {
            val body = response.body()
            val mutableList = body?.data
            if (mutableList != null) {
                adapter.items = mutableList
                adapter.notifyDataSetChanged()
            }

        }
    }


}
