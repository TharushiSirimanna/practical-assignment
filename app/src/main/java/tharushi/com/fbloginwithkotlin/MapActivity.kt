package tharushi.com.fbloginwithkotlin

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


const val EXTRA_LONGITUDE = "extra_longitude"
const val EXTRA_LATITUDE = "extra_latitude"
const val EXTRA_ADDRESS = "extra_address"

class MapActivity : AppCompatActivity(), OnMapReadyCallback {


    private var mMap: GoogleMap? = null
    private val TAG = "MapActivity"
    private var longitude: Double? = null
    private var latitude: Double? = null
    private var address: String? = null

    lateinit var mapFragment: SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        val actionbar = supportActionBar
        actionbar!!.title = "Map"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        longitude = intent.getDoubleExtra(EXTRA_LONGITUDE, 0.0)
        latitude = intent.getDoubleExtra(EXTRA_LATITUDE, 0.0)
        address = intent.getStringExtra(EXTRA_ADDRESS)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    override fun onStart() {
        mapFragment.onStart()
        super.onStart()
    }

    override fun onResume() {
        mapFragment.onResume()
        super.onResume()
    }

    override fun onPause() {
        mapFragment.onPause()
        super.onPause()
    }

    override fun onStop() {
        mapFragment.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onLowMemory() {
        mapFragment.onLowMemory()
        super.onLowMemory()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.isMyLocationEnabled = true
        val location = LatLng(latitude!!, longitude!!)
        mMap?.addMarker(MarkerOptions().snippet(address).position(location).title(address))
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(location))


    }

}
