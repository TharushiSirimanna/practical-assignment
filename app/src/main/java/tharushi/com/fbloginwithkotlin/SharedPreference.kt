package tharushi.com.fbloginwithkotlin

import android.content.Context
import android.content.SharedPreferences

class SharedPreference(val context: Context) {
    companion object {
        private val PREFS_NAME = "KotlinCodes"
        val KEY_NAME = "name"
        val KEY_EMAIL = "email"
    }

    val sharedPref: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun save(KEY: String, name: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(KEY, name)
        editor.apply()
    }

    fun getValueString(KEY: String): String? {
        return sharedPref.getString(KEY, null)
    }

    fun clearSharedPreferences() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.clear()
        editor.apply()
    }

}