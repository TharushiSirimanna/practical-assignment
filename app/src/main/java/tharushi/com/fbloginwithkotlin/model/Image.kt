package tharushi.com.fbloginwithkotlin.model

import android.os.Parcel
import android.os.Parcelable

class Image() : Parcelable {

    var small : String? = null
    var medium : String? = null
    var large : String? = null

    constructor(parcel: Parcel) : this() {
        small = parcel.readString()
        medium = parcel.readString()
        large = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(small)
        parcel.writeString(medium)
        parcel.writeString(large)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Image> {
        override fun createFromParcel(parcel: Parcel): Image {
            return Image(parcel)
        }

        override fun newArray(size: Int): Array<Image?> {
            return arrayOfNulls<Image?>(size)
        }
    }


}