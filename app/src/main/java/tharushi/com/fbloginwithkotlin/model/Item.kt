package tharushi.com.fbloginwithkotlin.model

import android.os.Parcel
import android.os.Parcelable

class Item() : Parcelable {
    var id : Int? = null
    var title : String? = null
    var description : String? = null
    var address : String? = null
    var postcode : String? = null
    var phoneNumber : String? = null
    var latitude : Double? = null
    var longitude : Double? = null
    var image : Image? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        description = parcel.readString()
        address = parcel.readString()
        postcode = parcel.readString()
        phoneNumber = parcel.readString()
        latitude = parcel.readValue(Double::class.java.classLoader) as? Double
        longitude = parcel.readValue(Double::class.java.classLoader) as? Double
        image = parcel.readValue(Image::class.java.classLoader) as? Image
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(address)
        parcel.writeString(postcode)
        parcel.writeString(phoneNumber)
        parcel.writeValue(latitude)
        parcel.writeValue(longitude)
        parcel.writeValue(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }
}