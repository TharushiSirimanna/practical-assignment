package tharushi.com.fbloginwithkotlin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import kotlinx.android.synthetic.main.image_list_item.view.*
import tharushi.com.fbloginwithkotlin.model.Item

class Adapter(var items: MutableList<Item>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var callback: AdapterCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.image_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).onBind(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        fun onBind(position: Int) {
            val item = items[position]
            itemView.tv_title.text = item.title
            itemView.tv_address.text = item.address
            Glide.with(itemView.context).load(item.image?.small).placeholder(R.drawable.gif)
                .apply(RequestOptions.circleCropTransform())
                .signature(ObjectKey(System.currentTimeMillis()))
                .into(itemView.imageView2)
            itemView.setOnClickListener {
                callback?.onClickViewCallback(items[adapterPosition])
            }
        }
    }

    interface AdapterCallback {
        fun onClickViewCallback(item: Item)
    }
}

