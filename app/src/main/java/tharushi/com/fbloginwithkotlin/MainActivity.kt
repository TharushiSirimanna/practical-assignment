package tharushi.com.fbloginwithkotlin

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import au.com.bse.presentation.util.Trubleshooting
import com.facebook.*
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    var callbackManager: CallbackManager? = null
    private lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreference = SharedPreference(this)

        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired
        if (isLoggedIn) {

            val intent = Intent(this@MainActivity, ListActivity::class.java)
            startActivity(intent)
            finish()

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

            println(Trubleshooting.generateHash(this))
        }

        callbackManager = CallbackManager.Factory.create()

        login_button.setPermissions("email")
        login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {

                result?.let {
                    val request =
                        GraphRequest.newMeRequest(result.accessToken) { obj, response ->


                            val intent = Intent(this@MainActivity, ListActivity::class.java)
                            val name = obj.getString("name")
                            val email = try {
                                obj.getString("email")
                            } catch (e: Exception) {
                                ""
                            }

                            sharedPreference.save(SharedPreference.KEY_NAME, name)
                            sharedPreference.save(SharedPreference.KEY_EMAIL,email)


                            intent.putExtra("name", name)
                            intent.putExtra("email", email)
                            startActivity(intent)
                            this@MainActivity.finish()
                        }
                    val parameters = Bundle()
                    parameters.putString("fields", "id, name, email")
                    request.parameters = parameters
                    request.executeAsync()
                }
            }

            override fun onCancel() {
                txt_hello.text = "Login canceled"

            }

            override fun onError(error: FacebookException?) {

            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)

    }
}
