package tharushi.com.fbloginwithkotlin

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey
import kotlinx.android.synthetic.main.activity_details.*
import tharushi.com.fbloginwithkotlin.model.Item

class DetailsActivity : AppCompatActivity() {

    private val TAG = "DetailsActivity"
    private var longitude: Double? = null
    private var latitude: Double? = null
    private  var address: String? = null
    private var  item: Item? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val actionbar = supportActionBar
        actionbar!!.title = "Details"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

         item = intent.getParcelableExtra<Item>(EXTRA_ITEM)

          tv_title.text = item?.title
          tv_description.text = item?.description
          longitude = item?.longitude
          latitude = item?.latitude
          address = item?.address


        Glide.with(this@DetailsActivity).load(item?.image?.small)
            .signature(ObjectKey(System.currentTimeMillis()))
            .into(iv_cat)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {

        R.id.action_map -> {
            startActivity()
            true


        }
        else ->  {

            super.onOptionsItemSelected(item)
        }

    }



    fun startActivity() {

        val intent = Intent(this@DetailsActivity, MapActivity::class.java)
        intent.putExtra(EXTRA_LONGITUDE, longitude)
        intent.putExtra(EXTRA_LATITUDE, latitude)
        intent.putExtra(EXTRA_ADDRESS, address)
        startActivity(intent)
    }

}
