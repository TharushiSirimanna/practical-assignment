package tharushi.com.fbloginwithkotlin

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import tharushi.com.fbloginwithkotlin.model.Response


val BASE_URL = "https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/"

public interface ApiService {

    @Headers(
        "Content-Type: application/json"
    )
    @GET("hotels.json")
    fun userLogIn(): Call<Response>


    object Creator {

        public fun  newApiService(): ApiService

        {

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }
}
